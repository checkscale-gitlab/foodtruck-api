# Foodtruck API
> An API to manage space reservations for foodtrucks

## Installation
1. Clone this repository
2. Go to the root of the project
3. If you are using WSL: `chmod +x bin/console`
4. `docker-compose up --build`

## API documentation  
Go to : [https://localhost/api](https://localhost/api)

## Built With
- [PHP 8.1](https://www.php.net/)
- [Symfony 5](https://symfony.com/)
- [API Platform](https://api-platform.com/)

## Author
- **Clément Lefrançois** - lefrancois.clement.18@gmail.com
