<?php

namespace App\Repository;

use App\Entity\Booking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\ParameterType;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    public function truckHasBookedInTheWeek(string $truckNumberplate, \DateTimeImmutable $bookingDate, int $id): bool
    {
        $result = $this->createQueryBuilder('b')
            ->where('b.truckNumberplate = :truckNumberplate')
            ->andWhere('b.bookingDate BETWEEN :monday AND :sunday')
            ->andWhere('b.id NOT IN (:id)')
            ->setParameter('truckNumberplate', $truckNumberplate, ParameterType::STRING)
            ->setParameter('monday', clone $bookingDate->modify('Monday this week'))
            ->setParameter('sunday', clone $bookingDate->modify('Sunday this week'))
            ->setParameter('id', $id, ParameterType::INTEGER)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return !empty($result);
    }
}
