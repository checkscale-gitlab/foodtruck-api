<?php

namespace App\Validator;

use App\Entity\Booking;
use App\Repository\BookingRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class BookingTruckConstraintValidator extends ConstraintValidator
{
    protected BookingRepository $bookingRepository;

    public function __construct(BookingRepository $bookingRepository)
    {
        $this->bookingRepository = $bookingRepository;
    }

    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint BookingTruckConstraint */

        if (null === $value || '' === $value) {
            return;
        }

        $booking = $this->context->getObject();

        if (!$booking instanceof Booking) {
            throw new \LogicException();
        }

        $bookingDate = \DateTimeImmutable::createFromMutable($booking->getBookingDate());

        if (!$this->bookingRepository->truckHasBookedInTheWeek($value, $bookingDate, $booking->getId())) {
            return;
        }

        $this->context
            ->buildViolation($constraint->message)
            ->addViolation();
    }
}
