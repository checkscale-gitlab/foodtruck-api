<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class BookingDateConstraint extends Constraint
{
    public string $message = 'There are no more places available for this date.';

    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
