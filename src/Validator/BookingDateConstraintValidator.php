<?php

namespace App\Validator;

use App\Repository\BookingRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class BookingDateConstraintValidator extends ConstraintValidator
{
    protected BookingRepository $bookingRepository;

    public function __construct(BookingRepository $bookingRepository)
    {
        $this->bookingRepository = $bookingRepository;
    }

    public function validate($value, Constraint $constraint)
    {
        /* @var $value \DateTimeInterface */
        /* @var $constraint BookingDateConstraint */

        if (null === $value || '' === $value) {
            return;
        }

        $maxAtDate = 5 === (int) $value->format('w') ? 6 : 7;

        if ($maxAtDate > $this->bookingRepository->count(['bookingDate' => $value])) {
            return;
        }

        $this->context
            ->buildViolation($constraint->message)
            ->addViolation();
    }
}
